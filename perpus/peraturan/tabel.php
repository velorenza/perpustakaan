<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <title>Tampil Barang</title>
  <link rel="stylesheet" href="../css/bootstrap.min.css" />
  <link rel="stylesheet" href="../DataTables/datatables.min.csss" />
  <link rel="stylesheet" type="text/css" href="../fontawesome/css/all.css" />
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/jquery.js"></script>
  
  <script src="../DataTables/datatables.min.js"></script>
  <link rel="stylesheet" type="text/css" href="../DataTables/DataTabless/css/jquery.dataTables.min.css" />
  <link rel="stylesheet" type="text/css" href="../DataTables/responsive.css" />
  <script src="../DataTables/DataTabless/js/jquery.dataTables.min.js"></script>
  <script src="../DataTables/responsive.js"></script>
  <script>

    $(document).ready( function () {
        $('#myTable').DataTable();
    } );
  </script>
  <style>
  .fa-plus-circle { 
  color: #33a008; 
  }
  .fa-trash-alt { 
  color: #b51515; 
  }
  .fa-print { 
  color: #bb7c18; 
  }
  
  
  </style>
</head>

<body>
  <?php
  include "../conf/koneksi.php";
  $tampil = mysqli_query($con, "SELECT * FROM tbl_peraturan");
  ?>
  <div class="container">
    <h2>Data Peraturan</h2>
        <a href="index.php"><i class="fas fa-plus-circle"></i></a>
        
        <p>
        
        <a href="../peraturan/index.php"> <button type='button' class='btn btn-info' >Tambah File Peraturan</button></a>
        
        </p>

    </p>

    <table id="myTable" class="display responsive nowrap" cellspacing="0" width="100%">

      <thead>
        <tr>
         <th>No</th>
          <th>Nama File</th>
          <th>Lokasi File</th>
          <th>Aksi</th>
          
        </tr>
      </thead>
      <tbody>

        <?php
        $no = 1;
        while ($r = mysqli_fetch_array($tampil)) {
        $nama = str_replace(" ", "%20", $r['lokasi_file']);
          echo "<tr>
          <td>$no</td>
           <td>$r[judul_peraturan]</td>
          <td>$r[lokasi_file]</td>
          <td>";
          ?>
          
        <a href=<?php echo '../peraturan/peraturan.php?pdf_file='. $nama ?>> 
        <button type='button' class='btn btn-info'>File Image</button>
        </a>

        
       <!--  <a href=<?php echo '../buku/edit.php?kode='. $r["kode_buku"] ?> >
        <button type='button' class='btn btn-primary'>Edit</button>
        </a> -->

        <a href=<?php echo '../peraturan/delete.php?id='. $r["id"] ?> >
        <button type='button' class='btn btn-danger'>Hapus</button>
        </a>
          </td>
          </tr>

          <?php
          $no++;
        } ?>
      </tbody>
    </table>
  </div>
</body>

</html>