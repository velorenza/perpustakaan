<!DOCTYPE html>
<?php
  include "../conf/koneksi.php";
  $cari = '';
  if (isset($_GET['cari'])){
    $cari=$_GET['cari'];
  }
  $query  = "SELECT * FROM tbl_buku WHERE judul_buku LIKE '%".$cari."%' OR lokasi_file LIKE '%".$cari."%'";
  $tampil = mysqli_query($con, $query);
?>
<html>
  <head>
     <!-- Basic -->
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <!-- Mobile Metas -->
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta name="viewport" content="initial-scale=1, maximum-scale=1">
     <!-- Site Metas -->
     <title>Perpustakaan</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/icon-rspw.png" type="image/x-icon" />
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="style.css">
    <!-- Colors CSS -->
    <link rel="stylesheet" href="css/colors.css">
    <!-- ALL VERSION CSS -->
    <link rel="stylesheet" href="css/versions.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Modernizer for Portfolio -->
    <script src="js/modernizer.js"></script>
    <link href="css/pagination.css" rel="stylesheet">

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.solodev.com/assets/pagination/jquery.twbsPagination.js"></script>
  </head>
   
  <script type="text/javascript">
    $(document).ready(function() {
      $('#pagination-demo').twbsPagination({
        totalPages: <?=$tampil->num_rows?>,
        // the current page that show on start
        startPage: 1,

        // maximum visible pages
        visiblePages: 5,

        initiateStartPageClick: true,

        // template for pagination links
        href: false,

        // variable name in href template for page number
        hrefVariable: '{{number}}',

        // Text labels
        first: 'First',
        prev: 'Previous',
        next: 'Next',
        last: 'Last',

        // carousel-style pagination
        loop: false,

        // callback function
        onPageClick: function (event, page) {
           $('.page-active').removeClass('page-active');
          $('#page'+page).addClass('page-active');
        },

        // pagination Classes
        paginationClass: 'pagination',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first',
        pageClass: 'page',
        activeClass: 'active',
        disabledClass: 'disabled',
      });
    });
  </script>
  <body>
  <!-- <div id="ebook" class="parallax section db" data-stellar-background-ratio="0.4" style="background:#fff;" data-scroll-id="ebook" tabindex="-1"> -->
    <div class="container">
      
      <div class="heading">
        <!-- <span class="icon-logo"><img src="images/icon-logo.png" alt="#"></span> -->
        <h2>This is a Collection of Books</h2>
      </div>
        <ul id="pagination-demo"></ul>
        <br />
      <?php 
        $page = 1;
        $index = 1;
        $newPage = true;
        while ($perimg=mysqli_fetch_array($tampil)){
          $nama = str_replace(" ", "%20", $perimg['lokasi_file']);
          if ($newPage) {
      ?>
            <div id="page<?=$page?>" class="page">
      <?php
            $page++;
            $newPage = false;
          }
      ?>       
              <div class="col-lg-2.5 col-md-2.5 col-sm-3 col-xs-3">
                <div class="widget clearfix">
                  <img src="../img/jurnal.jpg" alt="" class="img-responsive img-rounded">
                  <div class="widget-title">
                    <a href=<?php echo '../buku/pdf.php?pdf_file='. $nama ?>> 
                      <button type='button' class='btn btn-info'>File Image</button>
                    </a>
                    <!--  <a href="https://www.niagahoster.co.id/ebook">link</a> -->
                    <?php 
                      echo $perimg["lokasi_file"];
                    ?>              
                  </div>
                  <!-- end title -->
                  
                </div><!--widget -->
              </div>
      <?php
          if ($index == 4) {
            $newPage = true;
            $index = 0;
      ?>
            </div>
      <?php   
          }
          $index++;
        } 
      ?>


  </body>
</html>