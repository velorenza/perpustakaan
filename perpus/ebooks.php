<!DOCTYPE html>
<html lang="en">
   <!-- Basic -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <!-- Mobile Metas -->
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="viewport" content="initial-scale=1, maximum-scale=1">
   <!-- Site Metas -->
   <title>Perpustakaan</title>
   <meta name="keywords" content="">
   <meta name="description" content="">
   <meta name="author" content="">
   <!-- Site Icons -->
   <link rel="shortcut icon" href="images/icon-rspw.png" type="image/x-icon" />
   <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="/perpus/lifecare/css/bootstrap.min.css">
   <!-- Site CSS -->
   <link rel="stylesheet" href="/perpus/lifecare/style.css">
   <!-- Colors CSS -->
   <link rel="stylesheet" href="/perpus/lifecare/css/colors.css">
   <!-- ALL VERSION CSS -->
   <link rel="stylesheet" href="/perpus/lifecare/css/versions.css">
   <!-- Responsive CSS -->
   <link rel="stylesheet" href="/perpus/lifecare/css/responsive.css">
   <!-- Custom CSS -->
   <link rel="stylesheet" href="/perpus/lifecare/css/custom.css">
   <!-- Modernizer for Portfolio -->
   <script src="../lifecare/js/modernizer.js"></script>
   <!-- [if lt IE 9] -->
   </head>
   <body class="clinic_version">
      <!-- LOADER -->
      <div id="preloader">
         <img class="preloader" src="images/loaders/heart-loading2.gif" alt="">
      </div>
<!-- END LOADER -->
      <header>
         <div class="header-top wow fadeIn">
            <div class="container">
               <a class="navbar-brand" href="index.html"><img src="images/Logors.png" alt="image"></a>
               <div class="right-header">
                  <div class="header-info">
                     <!-- <div class="info-inner">
                        <span class="icontop"><img src="images/phone-icon.png" alt="#"></span>
                        <span class="iconcont"><a href="tel:800 123 456">800 123 456</a></span>	
                     </div>
                     <div class="info-inner">
                        <span class="icontop"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                        <span class="iconcont"><a data-scroll href="mailto:info@yoursite.com">info@Lifecare.com</a></span>	
                     </div>
                      <div class="info-inner">
                        <span class="icontop"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                        <span class="iconcont"><a data-scroll href="#">Daily: 7:00am - 8:00pm</a></span>	</div> -->
                  </div>
               </div>
            </div>
         </div>
         <div class="header-bottom wow fadeIn">
            <div class="container">
               <nav class="main-menu">
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i class="fa fa-bars" aria-hidden="true"></i></button>
                  </div>
				  
                  <div id="navbar" class="navbar-collapse collapse">
                     <ul class="nav navbar-nav">
                        <li><a class="active" href="index.html">Home</a></li>
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle effect-3" data-toggle="dropdown">E-Book
                           <b class="caret"></b>
                              </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="shop.html">Kedokteran</a>
                                    </li>
                                    <li>
                                        <a href="single_product.html">Keperawatan</a>
                                    </li>
                                    <li>
                                        <a href="checkout.html">Umum</a>
                                    </li>
                           
                                </ul>
                            </li>


                        <li><a data-scroll href="#service">Pedoman</a></li>
                        <li><a data-scroll href="#doctors">Peraturan</a></li>
                        <li><a data-scroll href="#price">Jurnal</a></li>
						      <!-- <li><a data-scroll href="#testimonials">Testimonials</a></li> -->
                        <li><a data-scroll href="#getintouch">Contact</a></li>
                     </ul>
                  </div>
               </nav>
               <div class="serch-bar">
                  <div id="custom-search-input">
                     <div class="input-group col-md-12">
                        <input type="text" class="form-control input-lg" placeholder="Search" />
                        <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                        </span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </header>
    </body>
    </html>